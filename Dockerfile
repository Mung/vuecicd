FROM node:14 as builder
WORKDIR /app
# Copy the package.json and install dependencies
COPY package*.json ./
RUN npm install
# Copy rest of the files
COPY . .
EXPOSE 8080
ENTRYPOINT ["npm", "run", "serve"]
