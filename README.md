# hello-world

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
[] Make a Dockerfile (install dependencies, copy the project file, expose port and run the project)
[] make Gitlab Ci file (to Run Docker file in pipeline, build and deploy the project)
[] docker-compose file (for Server for CD, specify ECR images to be use)
[] Inside server put a deploy.sh (restart the docker with the lastest code)